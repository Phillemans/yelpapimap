window.addEventListener("load", init);
let filterbox = document.getElementById("filters");
filterbox.addEventListener("click", function (event) {filterEvent(event)});

let map;
const root = "https://api.eet.nu/"
let link = "";
let markers = [];

function init() {
  initMap();

}

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: { lat: 53.4536672, lng: 3.5709125 },
    zoom: 16  ,
    minZoom: 10,
    maxZoom: 31,
    fullscreenControl: false,
    mapTypeControl: false,
    styles: [
      {
        featureType: "poi",
        stylers: [
         { visibility: "off" }
        ]   
       }
    ]
  });
  
  infoWindow = new google.maps.InfoWindow;

  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      link = root + "venues?geolocation=" + position.coords.latitude + "," + position.coords.longitude;

      infoWindow.setPosition(pos);
      infoWindow.setContent('Lokatie gevonden.');
      infoWindow.open(map);
      map.setCenter(pos);
      getToken();
    }, function () {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
}

function loadData(data){ 
  for (let i = 0; i < data.results.length; i++) {
    let longitude = data.results[i].geolocation.longitude;
    let latitude = data.results[i].geolocation.latitude;
    let restaurantNaam = data.results[i].name;
    let marker =new google.maps.Marker({
      position: { lat: latitude, lng: longitude},
      map: map,
      title: "Klik voor info over " +restaurantNaam,
      restaurantTag: data.results[i].category,
    });
    markers.push(marker);

    marker.addListener('click', function() {
      //put info in div
      let results = document.getElementById("results");
      results.style.height = "100%";


      let resNaam = document.getElementById('resNaam');          
      resNaam.innerHTML = data.results[i].name;

      let resCat = document.getElementById('resCat');
      resCat.innerHTML = "<b>soort keuken:</b> "+data.results[i].category;

      let adress = document.getElementById('adress');
      adress.innerHTML = "<b>adress:</b> "+data.results[i].address.street + " "+ data.results[i].address.city;

      let resTel = document.getElementById("resTel");
      if (data.results[i].telephone != null) {
        resTel.innerHTML = "<b>Tel:</b> " + data.results[i].telephone;
      }
      else if (data.results[i].mobile != null) {
        resTel.innerHTML = "<b>Mobiel:</b> " + data.results[i].mobile;
      }
      else {
        resTel.innerHTML = "Geen nummer beschikbaar";
      }

      let resWeb = document.getElementById("resWeb");
      if (data.results[i].website_url != null) {
        resWeb.innerHTML = "<b>Website:</b> <a href='" + data.results[i].website_url + "' target= '_blank'>" + data.results[i].website_url + "</a>";
      } else {
        resWeb.innerHTML = "";
      }

      let img = document.getElementById("image");
      if (data.results[i].images.cropped.length != 0) {
        img.setAttribute("src", data.results[i].images.cropped[0]);
      } else {
        img.setAttribute("src", "");
      }
    });
  }
}

function getToken(){
  $.getJSON(link)
    .done(loadData)
    .fail(function (){console.log("fail")});
}

function getFilters() {
  let filters = [];
  let boxes = document.getElementsByClassName("check");
  for (let i = 0; i < boxes.length; i++) {
    if (boxes[i].checked == true) {
      filters.push(boxes[i].value);
    }
  }
  showAllMarkers(false);
  if (filters.length == 0) {
    showAllMarkers(true);
  }
  for (let i = 0; i < filters.length; i++) {
    showMarkers(filters[i]);
  }


}

function showMarkers(category) {
  for (let i = 0; i < markers.length; i++) {
    if (markers[i].restaurantTag === category) {
      markers[i].setMap(map);
    }
  }
}

function showAllMarkers(boolean) {
  for (let i = 0; i < markers.length; i++) {
    if (boolean === true) {
      markers[i].setMap(map);
    } else {
      markers[i].setMap(null);
    }
  }
}

function filterEvent(event) {
  if (event.target.className == "check") {
    getFilters();
  }
}